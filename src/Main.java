import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    private final static int MAX_SIZE = 20;

    public static void main( String[] args ) {
        File file;
        int[] array;
        int current;

        if (args.length > 0) {
            file = new File(args[0]);
        } else {
            file = new File("123.csv");
        }
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))){
            
            ArrayList<Integer> list = new ArrayList<>();
            //наполнение динамического массива
            for (int i = 0; i < MAX_SIZE; i++) {
                list.add(i);
            }
            //перемешивание элементов
            Collections.shuffle(list);
            for (int i = 0; i < list.size(); i++) {
                if(i > 0) writer.write(",");
                writer.write(String.valueOf(list.get(i)));
            }
        }catch(IOException | NullPointerException e){
            e.printStackTrace();
        }

        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String[] strings = reader.readLine().split(",");
            array = massToInt(strings);
            // фильтрация массива
            current = array[0];
            System.out.print(current);
            for (int i = 1; i < array.length; i++) {
                if(current <= array[i]){
                    current = array[i];
                    System.out.print(",");
                    System.out.print(current);
                }
            }
            System.out.println();
            // конец
        }catch(IOException e){
            e.printStackTrace();
        }
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String[] strings = reader.readLine().split(",");
            array = massToInt(strings);
            // фильтрация массива
            current = array[0];
            System.out.print(current);
            for (int i = 1; i < array.length; i++) {
                if(current >= array[i]){
                    current = array[i];
                    System.out.print(",");
                    System.out.print(current);
                }
            }
            System.out.println();
            // конец
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    private static int[] massToInt(String[] stringMass) throws  IOException{
        int[] massInt = new int[stringMass.length];
        for (int i = 0; i < stringMass.length; i++) {
            massInt[i] = Integer.parseInt(stringMass[i]);
        }
        return massInt;
    }
}
